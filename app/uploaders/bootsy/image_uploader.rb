module Bootsy
  class ImageUploader < CarrierWave::Uploader::Base
    # include CarrierWave::MiniMagick

    # storage Bootsy.storage

    include Cloudinary::CarrierWave

    def store_dir
      "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end

    version :large do
      process :eager => true
      process resize_to_fit: [
                  700, 700
              ]

    end

    version :medium do
      process :eager => true
      process resize_to_fit: [
                  300, 300
              ]
    end

    version :small do
      process :eager => true
      process resize_to_fit: [
                  150, 150
              ]
    end

    version :thumb do
      process :eager => true
      process resize_to_fill: [150, 150]
    end


    def extension_white_list
      %w(jpg jpeg gif png)
    end
  end
end
